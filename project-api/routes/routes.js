const express = require('express');
const router = express.Router();

const { createUser, getAllUsers, getUsersByCategory, logInUser } = require('./../controller/getUserData');
const { getAllProjects, createProject } = require('./../controller/getProjectData');


router.post('/register', createUser);
router.get('/user', getAllUsers);
router.post('/user/category', getUsersByCategory);
router.post('/login', logInUser);

router.get('/project', getAllProjects);
router.post('/project', createProject);

module.exports = router;