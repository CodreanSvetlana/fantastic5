const { Evaluator } = require('./../models/classes');

const evaluator= {
    create: async (evaluator) => {
        try {
            const result = await Evaluator.create(evaluator);
            return result;    
        } catch(err) {
           throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const evaluators = await Evaluator.findAll();
            return evaluators;
        } catch(err) {
            throw new Error(err.message);
        }
    }
}

module.exports = evaluator;