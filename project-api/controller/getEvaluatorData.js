const evaluatorService = require('./../service/evaluator');

const createEvaluator = async (req, res, next) => {
    const evaluator = req.body;
    if(evaluator.projectId) {
        const result = await evaluatorService.create(evaluator);
        res.status(201).send({
            message: 'Evaluator added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid evaluator payload.'
        });
    }
}

const getAllEvaluators = async (req, res, next) => {
    try {
        const evaluator = await evaluatorService.getAll();
        res.status(200).send(evaluator);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

const getEvaluatorsByProjectId = async (req, res, next) => {
    try {
        const projectId = req.body.projectId;
        if(projectId) {
            try {
                const evaluators = await evaluatorService.getByProjectId(projectId);
                res.status(200).send(evaluators);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No projectId specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

module.exports = {
    createEvaluator,
    getAllEvaluators,
    getEvaluatorsByProjectId
}