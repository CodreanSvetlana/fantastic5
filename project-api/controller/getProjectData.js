const projectService = require('./../service/project');

const createProject = async (req, res, next) => {
    const project = req.body;
    if(project.projectName && project.teamName) {
        const result = await projectService.create(project);
        res.status(201).send({
            message: 'Project added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid project payload.'
        });
    }
}

const getAllProjects = async (req, res, next) => {
    try {
        const project = await projectService.getAll();
        res.status(200).send(project);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


module.exports = {
    createProject,
    getAllProjects,
}