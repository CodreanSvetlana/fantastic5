const userService = require('./../service/user');
const { sequelize, Project, User, Evaluator } = require('./../models/classes');

const createUser = async (req, res, next) => {
    const user = req.body;
    if(user.userName && user.password && user.category) {
        /*const result = await userService.create(user);
        res.status(201).send({
            message: 'User added successfully.'
        });*/
        User.findOne({where: {userName: user.userName}}).then(result => {
            if(result) {
              res.status(409).send({
                  message: "User already exists"
              }) 
            } else {
                User.build(user).save().then(user => {
                    res.status(201).send({
                        message: "User created successfully"
                    });
                })
            }
        })
    } else {
        res.status(400).send({
            message: 'Invalid user payload.'
        });
    }
}

const logInUser = (request, response) => {
    const credentials = request.body;
    User.findOne({where: {userName: credentials.userName, password: credentials.password}}).then(result => {
        if(result) {
            response.status(200).send(result);
        } else {
            response.status(404).send({
                message: 'Invalid credentials'
            })
        }
    })
}

const getAllUsers = async (req, res, next) => {
    try {
        const user = await userService.getAll();
        res.status(200).send(user);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

const getUsersByCategory = async (req, res, next) => {
    try {
        const category = req.body.category;
        if(category) {
            try {
                const users = await userService.getByCategory(category);
                res.status(200).send(users);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No category specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

module.exports = {
    createUser,
    getAllUsers,
    getUsersByCategory,
    logInUser
}