const configuration  = require('./../config/configuration.json');
const Sequelize = require('sequelize');

const DB_NAME = configuration.database.database_name;
const DB_USER = configuration.database.username;
const DB_PASS = configuration.database.password;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    dialect: 'mysql'
});

sequelize.authenticate().then(() => {
    console.log('Database connection success!');
}).catch(err => {
    console.log(`Database connection error: ${err}`);
});


class User extends Sequelize.Model { };

User.init({
    identifier: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    userName: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    category: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isValidPriority(value) {
                switch(value) {
                    case 'PROJECT MEMBER':
                        break;
                    case 'STUDENT':
                        break;
                    case 'PROFESSOR':
                        break;
                    default:
                     throw new Error("Priority value should be only: PROJECT MEMBER, STUDENT or PROFESSOR.");
                }
            }
        }
    }
}, {
    sequelize,
    modelName: 'users'
});



class Project extends Sequelize.Model { };
Project.init({
    identifier: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    projectName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    teamName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    grade: {
        type: Sequelize.INTEGER,
        allwoNull: true
    },
}, {
    sequelize,
    modelName: 'projects'
});


class Evaluator extends Sequelize.Model { };

Evaluator.init({
    identifier: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    }
}, {
    sequelize,
    modelName: 'evaluators'
});

Project.hasMany(User);
User.belongsTo(Project);

Project.hasMany(Evaluator);
Evaluator.belongsTo(Project);

User.hasMany(Evaluator);
Evaluator.belongsTo(User);


Project.sync(
    // {force: true}
    );
User.sync(
    // {force: true}
    );
Evaluator.sync(
    // {force: true}
    );


module.exports = {
    sequelize,
    Project,
    User,
    Evaluator
}